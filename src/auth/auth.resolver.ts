import { UseGuards } from '@nestjs/common';
import { Resolver, Query, Args, Mutation } from '@nestjs/graphql';
import { IUser } from 'src/users/interfaces/user.interface';
import { UserObjectType } from 'src/users/models/user.model';
import { AuthService } from './auth.service';
import { CurrentUser } from './decorators/current-user.decorator';
import { JwtGuard } from './guard/jwt.guard';
import { SigninInputType } from './inputs';
import { SigninOutputType } from './outputs';

@Resolver()
export class AuthResolver {
  constructor(private authService: AuthService) {}

  /* A mutation that takes in a signinInput and returns a user. */
  @Mutation(() => SigninOutputType)
  async signin(
    @Args('signinInput') signinInput: SigninInputType,
  ): Promise<IUser> {
    return await this.authService.signin(signinInput);
  }

  /* This is a query that returns a user. It is using the JwtGuard to protect the route. */
  @Query(() => UserObjectType)
  @UseGuards(JwtGuard)
  async getMe(@CurrentUser() user: IUser) {
    return user;
  }
}
