import * as moment from 'moment';
import { Injectable, BadRequestException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { IUser } from 'src/users/interfaces/user.interface';
import { UsersService } from 'src/users/users.service';
import { SigninInputType } from './inputs';

@Injectable()
export class AuthService {
  constructor(private jwt: JwtService, private userService: UsersService) {}

  /**
   * It takes a user's cnic and password, checks if the user exists, if the password matches, and then
   * signs a token and updates the user's session token
   * @param {SigninInputType} body - SigninInputType
   */
  async signin(body: SigninInputType): Promise<any> {
    // Check if user with cnic exist and password matches
    const user = await this.userService.findUserAndValidatePassword(
      body.cnic,
      body.password,
    );
    if (!user) {
      throw new BadRequestException('Invalid user or password');
    }

    const sessionToken = await this.signToken(user._id.toString(), user.cnic);

    await this.userService.updateUser(user._id.toString(), {
      lastLoggedIn: moment().unix(),
    });

    return {
      sessionToken,
      user: user as IUser,
    };
  }

  /**
   * It takes in an id and cnic and returns a signed JWT
   * @param {string} id - The user's id
   * @param {string} cnic - The cnic of the user.
   * @returns A JWT token
   */
  async signToken(id: string, cnic: string): Promise<string> {
    const payload = { id, cnic };
    return await this.jwt.signAsync(payload, {
      secret: process.env.JWT_SECRET,
    });
  }
}
