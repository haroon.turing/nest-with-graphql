import { Field, ObjectType } from '@nestjs/graphql';
import { UserObjectType } from 'src/users/models/user.model';

@ObjectType()
export class SigninOutputType {
  @Field(() => UserObjectType)
  user: UserObjectType;

  @Field()
  sessionToken: string;
}
