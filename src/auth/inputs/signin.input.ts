import { Field, InputType } from '@nestjs/graphql';
import { IsNotEmpty } from 'class-validator';
import { IsValidCNIC } from 'src/utils/decorators';
import { IsValidPassword } from 'src/utils/decorators/validation/isValidPassword.decorator';

@InputType()
export class SigninInputType {
  @Field()
  @IsNotEmpty()
  @IsValidCNIC()
  cnic: string;

  @Field()
  @IsNotEmpty()
  @IsValidPassword()
  password: string;
}
