import { Resolver, Query, Field, Int, ObjectType } from '@nestjs/graphql';

@Resolver()
export class AppResolver {
  @Query(() => [IntervalObjectType])
  sayHello(): any {
    return [
      {
        start: 132,
        end: 444,
      },
    ];
  }
}

@ObjectType()
export class IntervalObjectType {
  @Field(() => Int)
  start: number;

  @Field(() => Int)
  end: number;
}
