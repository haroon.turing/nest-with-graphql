import { UseGuards } from '@nestjs/common';
import { Resolver, Query, Args, Mutation } from '@nestjs/graphql';
import { JwtGuard } from 'src/auth/guard/jwt.guard';
import { ResponseType } from 'src/utils/shared/models/response.model';
import { CreateUserInputType } from './inputs';
import { UsersService } from './users.service';
import { PaginatedUserOutputType } from './outputs/paginated-user.output';
import { PaginationArgs } from 'src/utils/args';

@Resolver()
export class UsersResolver {
  constructor(private userService: UsersService) {}

  /* This is a mutation that takes in a CreateUserInputType and returns a ResponseType. */
  @Mutation(() => ResponseType)
  async createUser(@Args('newUserInput') newUserInput: CreateUserInputType) {
    return await this.userService.create(newUserInput);
  }

  /* This is a query that returns a PaginatedUserOutputType. */
  @Query(() => PaginatedUserOutputType, { nullable: true })
  @UseGuards(JwtGuard)
  async getUsers(@Args('paginationArgs') paginationArgs: PaginationArgs) {
    return await this.userService.findAll(paginationArgs);
  }
}
