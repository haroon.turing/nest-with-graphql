import { ObjectId } from 'mongoose';

export interface IUser {
  _id: ObjectId;
  name: string;
  password?: string;
  cnic: string;
  lastLoggedIn?: number;
  createdAt: number;
}
