import { Field, InputType, PartialType, PickType } from '@nestjs/graphql';
import { CreateUserInputType } from './create-user.input';

@InputType()
export class UpdateUserInputType extends PartialType(
  PickType(CreateUserInputType, ['name', 'password']),
) {
  @Field()
  lastLoggedIn?: number;
}
