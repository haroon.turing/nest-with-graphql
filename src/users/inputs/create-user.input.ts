import { Field, InputType } from '@nestjs/graphql';
import { IsNotEmpty, IsString } from 'class-validator';
import { IsValidCNIC } from 'src/utils/decorators';
import { IsValidPassword } from 'src/utils/decorators/validation/isValidPassword.decorator';

@InputType()
export class CreateUserInputType {
  @Field()
  @IsNotEmpty()
  @IsString()
  name: string;

  @Field()
  @IsNotEmpty()
  @IsValidCNIC()
  cnic: string;

  @Field()
  @IsNotEmpty()
  @IsValidPassword()
  password: string;
}
