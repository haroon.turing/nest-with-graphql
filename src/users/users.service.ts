import * as bcrypt from 'bcryptjs';
import {
  BadRequestException,
  Injectable,
  InternalServerErrorException,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateUserInputType } from './inputs';
import { IUser } from './interfaces/user.interface';
import { PaginationArgs } from 'src/utils/args';
import { IPaginatedResponse } from 'src/utils/interfaces';
import { UpdateUserInputType } from './inputs/update-user.input';

@Injectable()
export class UsersService {
  constructor(
    @InjectModel('Users')
    private UsersModel: Model<IUser>,
  ) {}

  /**
   * It takes a condition object as an argument, and returns a user object if the condition is met
   * @param {any} condition - any
   * @returns The user object
   */
  async findOne(condition: any) {
    try {
      if (!condition || !Object.keys(condition).length) return null;
      return (await this.UsersModel.findOne(condition, {
        password: 0,
      })) as IUser;
    } catch (error) {
      console.error(error);
    }
  }

  /**
   * If a user exist with this cnic throw exception, if user does not exist with this cnic create a new
   * user
   * @param {CreateUserInputType} newUserInput - CreateUserInputType
   */
  async create(newUserInput: CreateUserInputType) {
    // If a user exist with this cnic throw exception
    const doesUserExist = await this.UsersModel.findOne({
      cnic: newUserInput.cnic,
    });
    if (doesUserExist) {
      throw new BadRequestException('CNIC already registered!');
    }

    const salt = await bcrypt.genSaltSync(10);
    const hash = await bcrypt.hashSync(newUserInput.password, salt);

    // If user does not exist with this cnic create a new user
    const userCreated = await this.UsersModel.create({
      ...newUserInput,
      password: hash,
    });
    if (!userCreated) {
      throw new BadRequestException('Unable to create account');
    }
    return {
      success: 1,
      message: 'User created successfully',
    };
  }

  /**
   * It updates the session token of the user with the given id
   * @param {string} _id - The user's id
   * @param {string} sessionToken - The session token that will be stored in the database.
   * @returns The userUpdated object is being returned.
   */
  async updateSessionToken(_id: string, sessionToken: string) {
    const userUpdated = (await this.UsersModel.findByIdAndUpdate(
      _id,
      {
        sessionToken,
      },
      { new: true },
    )) as IUser;
    if (!userUpdated) {
      throw new InternalServerErrorException();
    }

    return userUpdated as IUser;
  }

  /**
   * It takes pagination arguments, creates a query, and then uses the query to find the users and
   * count the total number of users
   * @param {PaginationArgs} paginationArgs - PaginationArgs
   * @returns {
   *     nodes: users,
   *     totalCount: usersCount,
   *   } as IPaginatedResponse<IUser>;
   */
  async findAll(paginationArgs: PaginationArgs) {
    const findUsers = {
      $or: [
        { name: { $regex: paginationArgs.keyword, $options: 'i' } },
        { cnic: { $regex: paginationArgs.keyword, $options: 'i' } },
      ],
    };

    const pagination = [
      { $match: findUsers },
      { $skip: paginationArgs.offset },
      { $limit: paginationArgs.limit },
      // Todo: Sort is giving error
      // { $sort: sort },
    ];

    const [users, usersCount] = await Promise.all([
      this.UsersModel.aggregate(pagination).exec(),
      this.UsersModel.countDocuments(findUsers),
    ]);

    return {
      nodes: users,
      totalCount: usersCount,
    } as IPaginatedResponse<IUser>;
  }

  /**
   * It takes a cnic and password, finds the user with that cnic, and if the password matches, returns
   * the user
   * @param {string} cnic - string, password: string
   * @param {string} password - The password to be validated.
   * @returns - If cnic or password is not provided, null is returned
   *   - If user is not found, null is returned
   *   - If password is not equal, null is returned
   *   - If password is equal, user is returned
   */
  async findUserAndValidatePassword(cnic: string, password: string) {
    if (!cnic || !password) return null;

    const user = await this.UsersModel.findOne({ cnic });

    if (!user) return null;

    const isPasswordEqual = bcrypt.compareSync(password, user.password);
    if (isPasswordEqual) return user as IUser;
    else return null;
  }

  /**
   * It takes in an id and an updateUserInput object, and then it updates the user with the given id
   * with the given updateUserInput object
   * @param {string} _id - The id of the user to update
   * @param {UpdateUserInputType} updateUserInput - UpdateUserInputType
   */
  async updateUser(_id: string, updateUserInput: UpdateUserInputType) {
    await this.UsersModel.updateOne({ _id }, updateUserInput);
  }
}
