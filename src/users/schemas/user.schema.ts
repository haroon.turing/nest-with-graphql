import * as moment from 'moment';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type UserDocument = User & Document;

@Schema({
  timestamps: {
    currentTime: () => moment().unix(),
  },
})
export class User {
  @Prop({ default: '' })
  name: string;

  @Prop({ default: '' })
  password: string;

  @Prop({ required: true, unique: true, default: '' })
  cnic: string;

  @Prop()
  lastLoggedIn: number;

  @Prop()
  createdAt: number;

  @Prop()
  updatedAt: number;
}

export const UserSchema = SchemaFactory.createForClass(User);
