import { ObjectType, Field, Int } from '@nestjs/graphql';

@ObjectType()
export class UserObjectType {
  @Field()
  _id: string;

  @Field()
  name: string;

  @Field()
  cnic: string;

  @Field(() => Int, { nullable: true })
  lastLoggedIn?: number;

  @Field(() => String, { nullable: true })
  password?: string;
}
