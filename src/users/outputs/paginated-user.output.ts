import { Field, ObjectType } from '@nestjs/graphql';
import { UserObjectType } from '../models/user.model';

@ObjectType()
export class PaginatedUserOutputType {
  @Field(() => [UserObjectType])
  nodes: UserObjectType[];

  @Field()
  totalCount: number;
}
