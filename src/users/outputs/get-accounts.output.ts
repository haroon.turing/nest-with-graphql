import { Field, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class GetAccountsOutputType {
  @Field()
  accountNumber: string;
}
