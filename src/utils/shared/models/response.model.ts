import { Field, Int, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class ResponseType {
  @Field(() => Int)
  success: number;

  @Field()
  message: string;
}
