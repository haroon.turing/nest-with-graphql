export interface IPaginatedResponse<T> {
  nodes: [T];
  totalCount: number;
}
