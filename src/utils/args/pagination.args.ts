import { Field, InputType, Int } from '@nestjs/graphql';
import { IsNumber, IsString } from 'class-validator';

// Todo: Args type is not working here
@InputType()
export class PaginationArgs {
  @Field({ defaultValue: '' })
  @IsString()
  keyword: string;

  @Field(() => Int, { defaultValue: 0 })
  @IsNumber()
  offset: number;

  @Field(() => Int, { defaultValue: 10 })
  @IsNumber()
  limit: number;
}
