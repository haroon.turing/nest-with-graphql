import { registerDecorator, ValidationOptions } from 'class-validator';

const message =
  'Password should be 8 characters long and should contain at least 1 number, 1 special character and 1 upper case alphabet';

export function IsValidPassword(validationOptions?: ValidationOptions) {
  // eslint-disable-next-line @typescript-eslint/ban-types
  return function (object: Object, propertyName: string) {
    registerDecorator({
      name: 'isValidPassword',
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions || { message },
      validator: {
        validate(value: any) {
          return (
            typeof value === 'string' &&
            /**
             * Regex
             * - Should be a 8 digit string
             * - Should include special characters
             * - Should include digits
             * - Should include alphabets (uppercase + lowercase)
             */
            new RegExp(
              /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$/,
            ).test(value)
          );
        },
      },
    });
  };
}
