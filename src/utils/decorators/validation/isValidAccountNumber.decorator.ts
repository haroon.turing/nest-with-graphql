import { registerDecorator, ValidationOptions } from 'class-validator';

const message =
  'Account number should contain 13 digits followed by `ACC` and ends with number';

export function IsValidAccountNumber(validationOptions?: ValidationOptions) {
  // eslint-disable-next-line @typescript-eslint/ban-types
  return function (object: Object, propertyName: string) {
    registerDecorator({
      name: 'isValidAccountNumber',
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions || { message },
      validator: {
        validate(value: any) {
          return (
            typeof value === 'string' &&
            /**
             * Regex
             * - Should start with 13 digits
             * - After 13 digits it should have 'ACC'
             * - Ends with a digits
             */
            new RegExp(/^[0-9]{13}(ACC)[0-9]*$/).test(value)
          );
        },
      },
    });
  };
}
