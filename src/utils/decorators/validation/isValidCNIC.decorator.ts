import { registerDecorator, ValidationOptions } from 'class-validator';

const message = 'CNIC should contain 13 digits';

export function IsValidCNIC(validationOptions?: ValidationOptions) {
  // eslint-disable-next-line @typescript-eslint/ban-types
  return function (object: Object, propertyName: string) {
    registerDecorator({
      name: 'isValidCNIC',
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions || { message },
      validator: {
        validate(value: any) {
          return (
            typeof value === 'string' &&
            !isNaN(Number(value)) && // Is numeric string
            /**
             * Regex
             * - Should be a 13 digit string
             */
            new RegExp(/^[0-9]{13}$/).test(value)
          );
        },
      },
    });
  };
}
