import { ObjectType, Field } from '@nestjs/graphql';
import { AccountUserOutputType } from '../outputs';

@ObjectType()
export class AccountObjectType {
  @Field({ nullable: true })
  _id?: string;

  @Field(() => AccountUserOutputType, { nullable: true })
  user?: AccountUserOutputType;

  @Field()
  accountNumber: string;

  @Field({ nullable: true })
  balance: number;
}
