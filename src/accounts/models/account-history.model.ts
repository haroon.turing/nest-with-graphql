import { ObjectType, Field, ID } from '@nestjs/graphql';
import { EAccountHistoryType } from '../enums';

@ObjectType()
export class AccountHistoryObjectType {
  @Field(() => ID)
  _id: string;

  @Field()
  account1Id: string;

  @Field()
  account2Id: string;

  @Field()
  amount: number;

  @Field(() => EAccountHistoryType)
  type: EAccountHistoryType;

  @Field()
  createdAt: string;
}
