import { registerEnumType } from '@nestjs/graphql';

export enum EAccountHistoryType {
  DEPOSIT = 'DEPOSIT',
  WITHDRAW = 'WITHDRAW',
  TRANSFERRED = 'TRANSFERRED',
  RECEIVED = 'RECEIVED',
}

registerEnumType(EAccountHistoryType, {
  name: 'EAccountHistoryType',
  description: 'The status of account activity performed',
});
