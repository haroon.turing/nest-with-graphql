import mongoose from 'mongoose';
import * as moment from 'moment';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { User } from 'src/users/schemas/user.schema';

@Schema({
  timestamps: {
    currentTime: () => moment().unix(),
  },
})
export class Account {
  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'User' })
  userId: User;

  @Prop({ required: true, unique: true, uppercase: true })
  accountNumber: string;

  @Prop({ default: 0 })
  balance: number;

  @Prop()
  createdAt: number;

  @Prop()
  updatedAt: number;
}

export const AccountSchema = SchemaFactory.createForClass(Account);
