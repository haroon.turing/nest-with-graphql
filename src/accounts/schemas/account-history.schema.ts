import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import * as moment from 'moment';
import mongoose, { Document } from 'mongoose';
import { Account } from 'src/accounts/schemas/account.schema';
import { EAccountHistoryType } from '../enums';

export type HistoryDocument = History & Document;

@Schema({
  timestamps: {
    currentTime: () => moment().unix(),
  },
})
export class History {
  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'Accounts' })
  account1Id: Account;

  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'Accounts' })
  account2Id: Account;

  @Prop()
  amount: number;

  // 1: Deposit, 2: Withdraw, 3: Transfered, 4: Received
  @Prop({ enum: EAccountHistoryType })
  type: EAccountHistoryType;

  @Prop()
  createdAt: number;

  @Prop()
  updatedAt: number;
}

export const HistorySchema = SchemaFactory.createForClass(History);
