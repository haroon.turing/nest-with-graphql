import { ObjectId } from 'mongoose';
import { EAccountHistoryType } from '../enums';

export interface IAccountHistory {
  _id: ObjectId;
  account1Id: ObjectId;
  account2Id: ObjectId;
  amount: number;
  type: EAccountHistoryType;
}
