import { ObjectId } from 'mongoose';

export interface IAccount {
  _id: ObjectId;
  userId: string;
  accountNumber: string;
  balance: number;
}
