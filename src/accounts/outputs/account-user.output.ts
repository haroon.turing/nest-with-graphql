import { ObjectType, PartialType, PickType } from '@nestjs/graphql';
import { UserObjectType } from 'src/users/models/user.model';

@ObjectType()
export class AccountUserOutputType extends PartialType(
  PickType(UserObjectType, ['name', 'cnic']),
) {}
