import { Field, ObjectType, PartialType, PickType } from '@nestjs/graphql';
import { AccountHistoryObjectType } from '../models/account-history.model';
import { AccountObjectType } from '../models/account.model';

@ObjectType()
export class AccountHistoryOutputType extends PartialType(
  PickType(AccountHistoryObjectType, [
    '_id',
    'amount',
    'type',
    'createdAt',
  ] as const),
) {
  @Field(() => AccountObjectType)
  account1Id: AccountObjectType;

  @Field(() => AccountObjectType, { nullable: true })
  account2Id: AccountObjectType | null;
}
