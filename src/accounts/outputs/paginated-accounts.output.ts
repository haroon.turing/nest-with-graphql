import { Field, ObjectType } from '@nestjs/graphql';
import { AccountObjectType } from '../models/account.model';

@ObjectType()
export class PaginatedAccountsOutputType {
  @Field(() => [AccountObjectType])
  nodes: AccountObjectType[];

  @Field()
  totalCount: number;
}
