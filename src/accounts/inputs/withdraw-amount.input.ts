import { Field, InputType } from '@nestjs/graphql';
import { IsNotEmpty, IsNumber, Min } from 'class-validator';
import { IsValidAccountNumber } from 'src/utils/decorators';

@InputType()
export class WithdrawAmountInputType {
  @Field()
  @IsNotEmpty()
  @IsNumber()
  @Min(1)
  amount: number;

  @Field()
  @IsNotEmpty()
  @IsValidAccountNumber()
  accountNumber: string;
}
