import { InputType, PickType } from '@nestjs/graphql';
import { WithdrawAmountInputType } from './withdraw-amount.input';

@InputType()
export class AccountHistoryInputType extends PickType(WithdrawAmountInputType, [
  'accountNumber',
]) {}
