import { Field, InputType, PickType } from '@nestjs/graphql';
import { IsNotEmpty } from 'class-validator';
import { IsValidAccountNumber } from 'src/utils/decorators';
import { WithdrawAmountInputType } from './withdraw-amount.input';

@InputType()
export class TransferAmountInputType extends PickType(WithdrawAmountInputType, [
  'accountNumber',
  'amount',
]) {
  @Field()
  @IsNotEmpty()
  @IsValidAccountNumber({ message: 'Invalid reciever account number' })
  receiverAccountNumber: string;
}
