import { Field, InputType } from '@nestjs/graphql';
import { IsNotEmpty, IsNumber, Min } from 'class-validator';

@InputType()
export class CreateAccountInputType {
  @Field()
  @IsNotEmpty()
  @IsNumber()
  @Min(1)
  initialDepositAmount: number;
}
