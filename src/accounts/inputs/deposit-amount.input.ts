import { InputType, PickType } from '@nestjs/graphql';
import { WithdrawAmountInputType } from './withdraw-amount.input';

@InputType()
export class DepositAmountInputType extends PickType(WithdrawAmountInputType, [
  'accountNumber',
  'amount',
]) {}
