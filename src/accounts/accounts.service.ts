import * as moment from 'moment';
import {
  BadRequestException,
  Injectable,
  InternalServerErrorException,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { IUser } from 'src/users/interfaces/user.interface';
import {
  AccountHistoryInputType,
  CreateAccountInputType,
  DepositAmountInputType,
  TransferAmountInputType,
  WithdrawAmountInputType,
} from './inputs';
import { IAccount } from './interfaces/account.interface';
import { IAccountHistory } from './interfaces/account-history.interface';
import { EAccountHistoryType } from './enums';
import { PaginationArgs } from 'src/utils/args';
import { IPaginatedResponse } from 'src/utils/interfaces';
import { AccountObjectType } from './models/account.model';

@Injectable()
export class AccountsService {
  constructor(
    @InjectModel('Accounts')
    private AccountsModel: Model<IAccount>,
    @InjectModel('History')
    private HistoryModel: Model<IAccountHistory>,
  ) {}

  /**
   * It creates an account for a user
   * @param {IUser} user - IUser - The user object that is returned from the auth service.
   * @param {CreateAccountInputType} createAccountInput - CreateAccountInputType
   */
  async create(
    user: IUser,
    createAccountInput: CreateAccountInputType,
  ): Promise<AccountObjectType> {
    const accountNumber = user.cnic + 'ACC' + moment().unix();

    const isAccountCreated = await new this.AccountsModel({
      accountNumber,
      balance: createAccountInput.initialDepositAmount,
      userId: user._id,
    }).save();

    if (!isAccountCreated) {
      throw new InternalServerErrorException();
    }

    await new this.HistoryModel({
      account1Id: isAccountCreated._id,
      amount: createAccountInput.initialDepositAmount,
      type: EAccountHistoryType.DEPOSIT,
    }).save();

    return {
      accountNumber,
      balance: createAccountInput.initialDepositAmount,
    };
  }

  /**
   * It takes a user and a depositAmountInput as input, and returns a promise that resolves to an object with a
   * balance property
   * @param {IUser} user - IUser - this is the user object that is passed from the resolver.
   * @param {DepositAmountInputType} depositAmountInput - DepositAmountInputType
   * @returns The balance of the account
   */
  async deposit(
    user: IUser,
    depositAmountInput: DepositAmountInputType,
  ): Promise<AccountObjectType> {
    const isAccountUpdated = await this.AccountsModel.findOneAndUpdate(
      { userId: user._id, accountNumber: depositAmountInput.accountNumber },
      { $inc: { balance: depositAmountInput.amount } },
      { new: true },
    );

    if (!isAccountUpdated) this.throwError('Account not found');

    await new this.HistoryModel({
      account1Id: isAccountUpdated._id,
      amount: depositAmountInput.amount,
      type: EAccountHistoryType.DEPOSIT,
    }).save();

    return {
      accountNumber: depositAmountInput.accountNumber,
      balance: isAccountUpdated.balance,
    };
  }

  /**
   * It withdraws the amount from the account and updates the balance
   * @param {IUser} user - IUser - The user object that is passed from the resolver.
   * @param {WithdrawAmountInputType} withdrawAmountInput - WithdrawAmountInputType
   * @returns The balance of the account after the withdrawl
   */
  async withdraw(
    user: IUser,
    withdrawAmountInput: WithdrawAmountInputType,
  ): Promise<AccountObjectType> {
    const accountDetails = await this.AccountsModel.findOne({
      userId: user._id,
      accountNumber: withdrawAmountInput.accountNumber,
    });

    if (!accountDetails) this.throwError('Account not found');

    if (accountDetails.balance < withdrawAmountInput.amount) {
      this.throwError('Insufficient balance');
    }

    const isAccountUpdated = await this.AccountsModel.findOneAndUpdate(
      { _id: accountDetails._id, userId: user._id },
      { $inc: { balance: -withdrawAmountInput.amount } },
      { new: true },
    );

    if (!isAccountUpdated) this.throwError();

    await new this.HistoryModel({
      account1Id: accountDetails._id,
      amount: withdrawAmountInput.amount,
      type: EAccountHistoryType.WITHDRAW,
    }).save();

    return {
      accountNumber: withdrawAmountInput.accountNumber,
      balance: isAccountUpdated.balance,
    };
  }

  /**
   * It transfers the amount from one account to another
   * @param {IUser} user - IUser - The user object that is passed from the resolver.
   * @param {TransferAmountInputType} transferAmountInput - TransferAmountInputType
   * @returns The balance of the account after the transfer.
   */
  async transfer(
    user: IUser,
    transferAmountInput: TransferAmountInputType,
  ): Promise<AccountObjectType> {
    if (
      transferAmountInput.accountNumber ===
      transferAmountInput.receiverAccountNumber
    ) {
      this.throwError('Cannot transfer to same account');
    }

    const doesBeneficiaryExist = await this.AccountsModel.findOne({
      accountNumber: transferAmountInput.receiverAccountNumber,
    });
    if (!doesBeneficiaryExist) this.throwError('Beneficiary does not exist');

    const accountDetails = await this.AccountsModel.findOne({
      userId: user._id,
      accountNumber: transferAmountInput.accountNumber,
    });

    if (!accountDetails) this.throwError('Account not found');

    if (accountDetails.balance < transferAmountInput.amount) {
      this.throwError('Insufficient balance');
    }

    // Deducting amount
    const isAccountUpdated = await this.AccountsModel.findOneAndUpdate(
      { accountNumber: transferAmountInput.accountNumber },
      { $inc: { balance: -transferAmountInput.amount } },
      { new: true },
    );

    if (!isAccountUpdated) {
      await this.AccountsModel.updateOne(
        { accountNumber: transferAmountInput.accountNumber },
        { $inc: { balance: transferAmountInput.amount } },
      );

      this.throwError();
    }

    // Transferring amount
    const isBeneficiaryUpdated = await this.AccountsModel.updateOne(
      { accountNumber: transferAmountInput.receiverAccountNumber },
      { $inc: { balance: transferAmountInput.amount } },
    );

    // If not transferred then returning amount to user
    if (!isBeneficiaryUpdated) {
      await this.AccountsModel.updateOne(
        { accountNumber: transferAmountInput.accountNumber },
        { $inc: { balance: transferAmountInput.amount } },
      );

      this.throwError();
    }

    const transferHistoryToSave = [
      {
        account1Id: accountDetails._id,
        account2Id: doesBeneficiaryExist._id,
        amount: transferAmountInput.amount,
        type: EAccountHistoryType.TRANSFERRED,
      },
      {
        account1Id: doesBeneficiaryExist._id,
        account2Id: accountDetails._id,
        amount: transferAmountInput.amount,
        type: EAccountHistoryType.RECEIVED,
      },
    ];

    await this.HistoryModel.insertMany(transferHistoryToSave);

    return {
      accountNumber: transferAmountInput.accountNumber,
      balance: isAccountUpdated.balance,
    };
  }

  /**
   * It returns the history of a user's account
   * @param {IUser} user - IUser - This is the user object that is passed in from the resolver.
   * @param {AccountHistoryInputType} accountHistoryinput - AccountHistoryInputType
   * @returns The history of the account
   */
  async history(user: IUser, accountHistoryinput: AccountHistoryInputType) {
    const accountDetails = await this.AccountsModel.findOne({
      userId: user._id,
      accountNumber: accountHistoryinput.accountNumber,
    });

    if (!accountDetails) this.throwError('Account not found');

    const history = await this.HistoryModel.aggregate([
      {
        $match: {
          account1Id: accountDetails._id,
        },
      },
      {
        $lookup: {
          from: 'accounts',
          localField: 'account1Id',
          foreignField: '_id',
          as: 'account1Id',
        },
      },
      {
        $lookup: {
          from: 'accounts',
          localField: 'account2Id',
          foreignField: '_id',
          as: 'account2Id',
        },
      },
      {
        $unwind: {
          path: '$account1Id',
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $unwind: {
          path: '$account2Id',
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $project: {
          'account1Id.accountNumber': 1,
          'account2Id.accountNumber': 1,
          type: 1,
          createdAt: 1,
          amount: 1,
        },
      },
    ]);

    return history;
  }

  /**
   * It throws a BadRequestException with the message passed in as a parameter, or a default message if
   * no parameter is passed in
   * @param {string} [message] - The message to be displayed to the user.
   */
  throwError(message?: string) {
    throw new BadRequestException(message || 'An unknown error occured');
  }

  /**
   * It takes in a paginationArgs object, which contains the keyword, offset and limit. It then creates
   * a findAccounts object, which is used to find the accounts. The pagination array is then created,
   * which contains the findAccounts object, the offset and limit. The pagination array is then passed
   * to the aggregate function, which returns the users and the usersCount. The users and usersCount
   * are then returned as a paginated response
   * @param {PaginationArgs} paginationArgs - PaginationArgs
   */
  async findAll(paginationArgs: PaginationArgs) {
    const findAccounts = {
      accountNumber: { $regex: paginationArgs.keyword, $options: 'i' },
    };

    const lookup = {
      from: 'users',
      localField: 'userId',
      foreignField: '_id',
      as: 'user',
    };

    const unwind = {
      path: '$user',
      preserveNullAndEmptyArrays: true,
    };

    const pagination = [
      { $match: findAccounts },
      { $lookup: lookup },
      { $unwind: unwind },
      { $skip: paginationArgs.offset },
      { $limit: paginationArgs.limit },
      // Todo: Sort is giving error
      // { $sort: sort },
    ];

    const [users, usersCount] = await Promise.all([
      this.AccountsModel.aggregate(pagination).exec(),
      this.AccountsModel.countDocuments(findAccounts),
    ]);

    return {
      nodes: users,
      totalCount: usersCount,
    } as IPaginatedResponse<IAccount>;
  }

  /**
   * It returns a list of account numbers for a given user
   * @param {IUser} user - IUser - this is the user object that is passed in from the controller.
   * @returns An array of account numbers
   */
  async getMyAccounts(user: IUser) {
    const accounts = await this.AccountsModel.find(
      { userId: user._id },
      { accountNumber: 1, _id: 0 },
    );

    return accounts;
  }
}
