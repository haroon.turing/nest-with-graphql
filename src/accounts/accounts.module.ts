import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AccountsResolver } from './accounts.resolver';
import { AccountsService } from './accounts.service';
import { HistorySchema } from './schemas/account-history.schema';
import { AccountSchema } from './schemas/account.schema';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'Accounts', schema: AccountSchema }]),
    MongooseModule.forFeature([{ name: 'History', schema: HistorySchema }]),
  ],
  providers: [AccountsService, AccountsResolver],
})
export class AccountsModule {}
