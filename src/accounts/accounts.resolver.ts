import { UseGuards } from '@nestjs/common';
import { Resolver, Query, Args, Mutation } from '@nestjs/graphql';
import { CurrentUser } from 'src/auth/decorators/current-user.decorator';
import { JwtGuard } from 'src/auth/guard/jwt.guard';
import { IUser } from 'src/users/interfaces/user.interface';
import { GetAccountsOutputType } from 'src/users/outputs/get-accounts.output';
import { PaginationArgs } from 'src/utils/args';
import { AccountsService } from './accounts.service';
import {
  AccountHistoryInputType,
  CreateAccountInputType,
  DepositAmountInputType,
  TransferAmountInputType,
  WithdrawAmountInputType,
} from './inputs';
import { AccountObjectType } from './models/account.model';
import { AccountHistoryOutputType } from './outputs';
import { PaginatedAccountsOutputType } from './outputs/paginated-accounts.output';

@Resolver()
export class AccountsResolver {
  constructor(private accountsService: AccountsService) {}

  /* This is a mutation that creates an account. */
  @Mutation(() => AccountObjectType)
  @UseGuards(JwtGuard)
  async createAccount(
    @Args('createAccountInput') createAccountInput: CreateAccountInputType,
    @CurrentUser() user: IUser,
  ): Promise<AccountObjectType> {
    return await this.accountsService.create(user, createAccountInput);
  }

  /* This is a mutation that deposits an amount into an account. */
  @Mutation(() => AccountObjectType)
  @UseGuards(JwtGuard)
  async depositAmount(
    @Args('depositAmountInput') depositAmountInput: DepositAmountInputType,
    @CurrentUser() user: IUser,
  ): Promise<AccountObjectType> {
    return await this.accountsService.deposit(user, depositAmountInput);
  }

  /* This is a mutation that withdraws an amount from an account. */
  @Mutation(() => AccountObjectType)
  @UseGuards(JwtGuard)
  async withdrawAmount(
    @Args('withdrawAmountInput') withdrawAmountInput: WithdrawAmountInputType,
    @CurrentUser() user: IUser,
  ): Promise<AccountObjectType> {
    return await this.accountsService.withdraw(user, withdrawAmountInput);
  }

  /* This is a mutation that transfers an amount from one account to another. */
  @Mutation(() => AccountObjectType)
  @UseGuards(JwtGuard)
  async transferAmount(
    @Args('transferAmountInput') transferAmountInput: TransferAmountInputType,
    @CurrentUser() user: IUser,
  ): Promise<AccountObjectType> {
    return await this.accountsService.transfer(user, transferAmountInput);
  }

  /* This is a query that returns the history of an account. */
  @Query(() => [AccountHistoryOutputType])
  @UseGuards(JwtGuard)
  async accountHistory(
    @Args('accountHistoryInput') accountHistoryInput: AccountHistoryInputType,
    @CurrentUser() user: IUser,
  ): Promise<any> {
    return await this.accountsService.history(user, accountHistoryInput);
  }

  /* This is a query that returns a paginated list of accounts. */
  @Query(() => PaginatedAccountsOutputType)
  @UseGuards(JwtGuard)
  async getAllAccounts(@Args('paginationArgs') paginationArgs: PaginationArgs) {
    return await this.accountsService.findAll(paginationArgs);
  }

  /* This is a query that returns an array of GetAccountsOutputType. */
  @Query(() => [GetAccountsOutputType])
  @UseGuards(JwtGuard)
  async getMyAccounts(@CurrentUser() user: IUser) {
    return await this.accountsService.getMyAccounts(user);
  }
}
